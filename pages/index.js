import React, { Component, useEffect } from "react";

class MyComponent extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         error: null,
         isLoaded: false,
         bpi: {}
      };
   }

   componentDidMount() {
      fetch("https://api.coindesk.com/v1/bpi/currentprice.json")
         .then(res => res.json())
         .then(
            (result) => {
               this.setState({
                  isLoaded: true,
                  bpi: result.bpi
               });
            },
            (error) => {
               this.setState({
                  isLoaded: true,
                  error
               });
            }
         );
   }

   render() {
      const { error, isLoaded, bpi } = this.state;
      if (error) {
         return 'Error';
      } else if (!isLoaded) {
         return 'Loading...';
      } else {
         return (
            <div className="wrapper">
               <span className="wrapper__title">Bitcoin price</span>
               <div className="wrapper__section" key={bpi?.USD} >
                  <span className="wrapper__section-descr">
                     {bpi?.USD.code} rate: {bpi?.USD.rate} rate float: {bpi?.USD.rate_float}
                  </span>
                  <span className="wrapper__section-descr">
                     {bpi?.GBP.code} rate: {bpi?.GBP.rate} rate float: {bpi?.GBP.rate_float}
                  </span>
                  <span className="wrapper__section-descr">
                     {bpi?.EUR.code} rate: {bpi?.EUR.rate} rate float: {bpi?.EUR.rate_float}
                  </span>
               </div>
            </div>
         );
      }
   }
}


export default MyComponent;

setInterval(() => { window.location.reload(); }, 60000);


